# TIM WIOS Basic API Server - Setup Guide

> Before starting the setup, make sure you have installed docker and docker-compose.
> Follow the steps to complete this demo setup successfully.

## Requirements

First you need [Docker Compose](https://docs.docker.com/compose/install/) to run the server on your local machine. Docker Compose relies to Docker Engine, so make sure you have Docker Engine and Docker Compose installed either.

```sh
~$ docker-compose --version
docker-compose version 1.24.0, build 0aa59064
```

## 1. Move into Server directory

Download the project files into your machine

```sh
cd ~/[parent_dir]/[project_dir]/
```

## 2. Run the docker-compose command  

Open up your terminal and move into the downloaded repo folder.
Then run the command below;

```sh
docker-compose up -d --build
```

## Summary

It's that easy to run the demo server in your machine.  
You can fully manage and manipulate database and server either.

**Note:** After you make changes to the project source files, make sure to run the following command to apply changes to docker containers.
`docker-compose up -d --build --force-recreate`
