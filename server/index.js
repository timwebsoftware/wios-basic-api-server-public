const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const compression = require('compression')
const helmet = require('helmet')
const { host, port } = require('./config')
const routes = require('./routes')

const app = express()

// set Express port
app.set('port', port)

// db connection
require('./models')

// CORS config
app.use(cors({
  origin: '*',
  optionsSuccessStatus: 200,
  credentials: true,
  methods: 'POST',
  allowedHeaders: ['Content-Type']
}))

// security headers middleware
app.use(helmet())

// request body parser middleware
app.use(bodyParser.json())

// compacting requests using GZIP middleware
app.use(compression())

// import Routes
app.use('/', routes)

// Listen the server
app.listen(port, host)
console.info('Server listening on http://' + host + ':' + port)
