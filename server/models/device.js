module.exports = (sequelize, DataTypes) => {
  const Device = sequelize.define('device', {
    device_id: {
      field: 'device_id',
      type: DataTypes.STRING,
      primaryKey: true,
      unique: true,
      allowNull: false
    },
    page_location: {
      field: 'page_location',
      type: DataTypes.STRING,
      default: null
    },
    last_active_time: {
      field: 'last_active_time',
      type: DataTypes.DATE,
      defaultValue: sequelize.NOW
    }
  }, {
    underscored: true
  })

  return Device
}
