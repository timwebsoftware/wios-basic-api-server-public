module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('product', {
    barcode: {
      field: 'barcode',
      type: DataTypes.STRING,
      primaryKey: true,
      unique: true,
      allowNull: false
    },
    entered_by: {
      field: 'entered_by',
      type: DataTypes.STRING,
      allowNull: false
    },
    entered_at: {
      field: 'entered_at',
      type: DataTypes.DATE,
      defaultValue: sequelize.NOW
    }
  }, {
    underscored: true
  })

  Product.associate = (models) => {
    Product.hasMany(models.operation_log, {
      foreignKey: 'fk_product'
    })
  }

  return Product
}
