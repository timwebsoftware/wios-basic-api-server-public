const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')
const basename = path.basename(module.filename)
const { database: dbConfig } = require('../config')

// add databases
const db = new Sequelize(dbConfig.name, dbConfig.username, dbConfig.password, {
  dialect: dbConfig.dialect,
  host: dbConfig.host,
  port: dbConfig.port,
  autoreconnect: dbConfig.autoreconnect,
  logging: dbConfig.logging
})

// add models of the db
fs
  .readdirSync(__dirname)
  .filter((file) =>
    (file.indexOf('.') !== 0) &&
    (file !== basename) &&
    (file.slice(-3) === '.js'))
  .forEach((file) => {
    const model = db.import(path.join(__dirname, file))
    db[model.name] = model
  })

for (let key in db) {
  if (db[key].associate) {
    db[key].associate(db)
  }
}

db.authenticate()
  .then(() => {
    console.log('DB connected')
  })
  .catch((err) => {
    console.log(new Error(err))
    process.exit(1)
  })
// sync method is only for development purposes
db.sync({ alter: false, force: false })
  .then(() => {
    console.log('DB synced')
  })
  .catch((err) => {
    console.log(new Error(err))
    process.exit(1)
  })

module.exports = db
