module.exports = (sequelize, DataTypes) => {
  const OperationLog = sequelize.define('operation_log', {
    op_name: {
      field: 'op_name',
      type: DataTypes.STRING,
      allowNull: false
    },
    did_success: {
      field: 'did_success',
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    done_at: {
      field: 'done_at',
      type: DataTypes.DATE,
      defaultValue: sequelize.NOW
    }
  }, {
    underscored: true
  })

  OperationLog.associate = (models) => {
    OperationLog.belongsTo(models.device, {
      foreignKey: 'fk_device',
      as: 'device'
    })
    OperationLog.belongsTo(models.product, {
      foreignKey: 'fk_product',
      as: 'product'
    })
  }

  return OperationLog
}
