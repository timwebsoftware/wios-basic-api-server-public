const Device = require('../models').device
const sendResponse = require('../helpers/responseSender')
/**
  * @desc checks device with device_id in db
  * @param object req - http request
  * @param object res - http response
  * @param function next - callback function
  * @returns void
*/
module.exports = async function checkDevice (req, res, next) {
  var device_id
  if (req.method === 'GET') {
    device_id = req.query.id
  }
  else if (req.method === 'POST') {
    device_id = req.body.device_id
  }
  try {
    // query device in db with device_id
    const device = await Device.findOne({
      where: {
        device_id
      }
    })
    if (!device) {
      // create a new device
      await Device.create({
        device_id
      })
    }
    // update device last active time
    await Device.update({
      last_active_time: new Date()
    }, {
      where: {
        device_id
      }
    })
    // call next process
    return next()
  } catch (err) {
    sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: 500,
        message: err.message
      }
    })
  }
}
