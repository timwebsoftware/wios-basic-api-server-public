const Device = require('../models').device
const sendResponse = require('../helpers/responseSender')

/**
  * @desc updates device page location and sends device info
  * @param req object - http request
  * @param res object - http response
  * @returns void - sendResponse function call
*/
async function select (req, res) {
  const clientIP = req.header('x-forwarded-for') || req.connection.remoteAddress
  var device_id
  if (req.method === 'GET') {
    device_id = req.query.id
  }
  else if (req.method === 'POST') {
    device_id = req.body.device_id
  }

  try {
    // update device page location
    await Device.update({
      page_location: 'device_info'
    }, {
      where: {
        device_id
      }
    })

    return sendResponse(res, 'info', {
      screen: {
        top_info: 'Device Info',
        text: `Device ID:\n${device_id}\nIP Address:\n${clientIP}`,
        icon: 'none',
        touch_button: {
          id: 'menu',
          name: 'Menu'
        }
      },
      locked: true
    })
  } catch (err) {
    sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: 500,
        message: err.message
      }
    })
  }
}

module.exports = { select }
