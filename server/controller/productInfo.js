const Device = require('../models').device
const Product = require('../models').product
const OperationLog = require('../models').operation_log
const sendResponse = require('../helpers/responseSender')
/**
  * @desc updates device page location
  * @param req object - http request
  * @param res object - http response
  * @returns void - sendResponse function call
*/
async function select (req, res) {
  var device_id
  var param
  if (req.method === 'GET') {
    device_id = req.query.id
  }
  else if (req.method === 'POST') {
    device_id = req.body.device_id
    param = req.body.param
  }

  try {
    if (req.method === 'GET') {
      const device = await Device.findOne({
        where: { device_id }
      })
      param = device.page_location
    }
    // update device page location
    await Device.update({
      page_location: param
    }, {
      where: {
        device_id
      }
    })
    return sendResponse(res, 'info', {
      screen: {
        text: 'Scan Product \nBarcode',
        top_info: 'Product Info',
        touch_button: {
          id: 'menu',
          name: 'Menu'
        }
      }
    })
  } catch (err) {
    sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: 500,
        message: err.message
      }
    })
  }
}

/**
  * @desc updates device page location and creates operation log
  * @param req object - http request
  * @param res object - http response
  * @returns void - sendResponse function call
*/
async function scan (req, res) {
  const {
    param,
    device_id
  } = req.body
  let op_success = false

  try {
    const device = await Device.findOne({
      where: {
        device_id
      }
    })
    const product = await Product.findOne({
      where: {
        barcode: param
      }
    })
    // if product is found
    if (product) {
      op_success = true   
    }

    await OperationLog.create({
      op_name: device.page_location,
      fk_device: device.device_id,
      fk_product: op_success ? product.barcode : null,
      did_success: op_success,
      done_at: new Date()
    })

    if (!op_success) {
      return sendResponse(res, 'error', {
        screen: {
          text: 'Unknown \nProduct',
          top_info: 'Product Info',
          touch_button: {
            id: 'menu',
            name: 'Menu'
          }
        }
      })
    }

    return sendResponse(res, 'success', {
      screen: {
        icon: 'ok',
        text: `Entered By:\n${product.entered_by}\nEntered At:\n${getDate(product.entered_at)}`,
        top_info: 'Product Info',
        touch_button: {
          id: 'menu',
          name: 'Menu'
        }
      }
    })
  } catch (err) {
    sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: 500,
        message: err.message
      }
    })
  }
}

function getDate (d) {
  let min = new Date(d).getMinutes()
  let hour = new Date(d).getHours()
  let day = new Date(d).getDate()
  let month = new Date(d).getMonth() + 1

  min = min < 10 ? `0${min}` : min
  hour = hour < 10 ? `0${hour}` : hour
  day = day < 10 ? `0${day}` : day
  month = month < 10 ? `0${month}` : month

  return `${hour}:${min} ${day}/${month}`
}

module.exports = {
  select,
  scan
}
