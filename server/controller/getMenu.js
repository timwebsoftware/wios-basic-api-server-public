const Device = require('../models').device
const sendResponse = require('../helpers/responseSender')
const operations = [
  {
    id: 'product_info',
    name: 'Product Info'
  },
  {
    id: 'product_entry',
    name: 'Product Entry'
  }
]

/**
  * @desc updates device page location and binds menu items into response
  * @param req object - http request
  * @param res object - http response
  * @returns void - sendResponse function call
*/
async function select (req, res) {
  var device_id
  if (req.method === 'GET') {
    device_id = req.query.id
  }
  else if (req.method === 'POST') {
    device_id = req.body.device_id
  }

  try {
    // update device page location
    await Device.update({
      page_location: 'menu'
    }, {
      where: {
        device_id
      }
    })

    return sendResponse(res, 'info', {
      screen: {
        menu: operations,
        top_info: device_id,
        touch_button: {
          id: 'device_info',
          name: 'Device Info'
        }
      },
      locked: true
    })
  } catch (err) {
    sendResponse(res, 'error', {
      screen: {
        icon: 'err',
        text: 'Internal \nServer Error'
      },
      error: {
        code: 500,
        message: err.message
      }
    })
  }
}

module.exports = { select }
