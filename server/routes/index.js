const router = require('express').Router()
const Device = require('../models').device
const checkData = require('../middleware/dataChecker')
const checkDevice = require('../middleware/deviceChecker')
const sendResponse = require('../helpers/responseSender')
const {
  select: processMenuSelection
} = require('../controller/getMenu')
const {
  select: select_product_info,
  scan: scan_product_info
} = require('../controller/productInfo')
const {
  select: select_product_entry,
  scan: scan_product_entry
} = require('../controller/productEntry')
const {
  select: select_device_info
} = require('../controller/deviceInfo')

// merging is needed to access easily to operation controllers
const operations = {
  select_product_info,
  scan_product_info,
  select_product_entry,
  scan_product_entry,
  select_device_info
}

router.get('/', checkData, checkDevice, async (req, res) => {
  const device_id = req.query.id

  const device = await Device.findOne({
    where: {
      device_id
    }
  })

  if (`select_${device.page_location}` in operations) {
    // return to operation controller
    return operations[`select_${device.page_location}`](req, res)
  }
  // if page location is menu or any other falsy value
  // return menu selection
  return processMenuSelection(req, res)
})

router.post('/', checkData, checkDevice, async (req, res) => {
  const {
    action,
    param,
    device_id
  } = req.body

  const device = await Device.findOne({
    where: {
      device_id
    }
  })

  if (action === 'select') {
    // process operation select
    if (`select_${param}` in operations) {
      // return to operation controller
      return operations[`select_${param}`](req, res)
    }
    // if page location is menu or any other falsy value
    // return menu selection
    return processMenuSelection(req, res)
  }
  else if (action === 'scan') {
    // process operation scan
    if (`scan_${device.page_location}` in operations) {
      // return to operation controller
      return operations[`scan_${device.page_location}`](req, res)
    }
    // In any other case
    // return menu selection
    return processMenuSelection(req, res)
  }
  // return action value error
  return sendResponse(res, 'error', {
    screen: {
      icon: 'error',
      text: 'Invalid \nAction Type'
    },
    error: {
      code: 400,
      message: 'The request body action value is not recognized'
    }
  })
})

module.exports = router
